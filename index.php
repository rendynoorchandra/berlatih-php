<?php

require_once('animal.php');

$sheep = new Animal("Shaun");

echo "Nama Hewan: " . $sheep->name . "<br>" ; // "shaun"
echo "Jumlah Kaki :  $sheep->legs <br>"; // 4
echo "Cold Blooded: $sheep->cold_blooded <br><br>"; // "no"

require('frog.php');
require('ape.php');
$sungokong = new Ape("kera sakti");
// $sungokong->yell() // "Auooo"

$kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"

echo "Nama Hewan: " . $kodok->name . "<br>";
echo "Jumlah Kaki: ". $kodok->legs . "<br>";
echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
$kodok->jump();
echo "<br><br>";


echo "Nama Hewan: " . $sungokong->name . "<br>";
echo "Jumlah Kaki: " . $sungokong->legs . "<br>";
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
$sungokong->yell() . "<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>